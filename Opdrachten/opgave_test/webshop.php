<?php
	include_once('classes/Order.class.php');
	
	try{
	if(isset($_POST['bt_submin']))
	{
		$order = new Order;
		$order->Name = $_POST['name'];
		$order->HowMany = $_POST['howmany'];
		$order->Product = $_POST['product'];
		$order->Save();
		$succes = true;
	}
	
	} catch (Exception $e) {
		$feedback = $e->getMessage();
		//echo $feedback;
	}



?><!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>Test OOP</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="humans.txt">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

    <link rel="stylesheet" href="css/gumby.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="js/libs/modernizr-2.6.2.min.js"></script>
</head>

<body>

<div class="container">

  	<div class="row">
  	  <div class="twelve columns special head">
  	  	<h1>Testground</h1>
  	  </div>
  	</div>

  	<div class="row">
        <div class="four columns"> 
        <p>Zorg dat we een bestelling kunnen plaatsen.</p>
        <h3>Voorwaarden</h3>
        <ol>
            <li>naam mag niét leeg zijn (2 punten)</li>
            <li>bescherm tegen XSS en SQL Injection (2 punten)</li>
            <li>er kan maximaal voor 120 Euro per keer besteld worden. (4 punten)</li>
            <li>print alle bestellingen onderaan uit (4 punten)</li>
            <li>zorg dat fout- of succesboodschappen énkel op de juiste ogenblikken getoond worden. (4 punten)</li>
            <li>zorg dat de bestelling correct in de databank terecht komt (zie voorbeeld data in de databank) (4 punten)</li>
        </ol>
        </div>
        
            
        <div class="eight columns">
            <?php
            if ($feedback) {
            	echo "<div class='danger alert'>". $feedback . "</div>";
            	}  
            
            if ($succes)
            	{
            	echo "<div class='success alert'> Bedankt, je bestelling is geplaatst</div>";
            	}
            	
            ?>
            
            <form method="post" action="">    
              <ul>
                <li class="field"><input name="name" class="text input" type="text" placeholder="Uw naam" /></li>
                <li class="field">
                <div class="picker">
                    <select name="product">
                        <option value="" disabled>Selecteer een product</option>
                        <option value="product1">Product number 1 (10 &euro;)</option>
                        <option value="product2">Product number 2 (20 &euro;)</option>
                    </select>            
                </div>
                </li>
                <li class="field"><input name="howmany" class="number input" type="number" placeholder="Hoeveel stuks?" /></li>
                
                <div class="pretty medium primary btn">
                <input type="submit" value="Bestelling plaatsen" name="bt_submin" />
                </div>
              </ul>
            </form>
            
            <hr />
            
            <h2>Uw bestellingen:</h2>
            <ul>
                <li>Bill Gates : 5 x product1 = 50.00€</Ga></li>
            </ul>             
        </div>    
        
    </div> 

	</div> <!--! end of #container -->

</section>

  <script src="js/libs/gumby.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>

  </body>
</html>
