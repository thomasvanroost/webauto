<?php

	include_once("db.class.php");
	class Tweet
	{
		private $m_sContent;
		private $m_dTweetDate;
		private $m_sUser;
	
		function __set($p_sProperty, $v_vValue)
		{
			switch ($p_sProperty)
			{
				case "Content":
				$this->m_sContent = $v_vValue; 
				break;
				
				case "TweetDate":
				$this->m_dTweetDate = $v_vValue;
				break;
				
				case "User":
				$this->m_sUser= $v_vValue;
				break;
				
			}
			
		}	
		
		function __get($p_sProperty)
		{
			switch ($p_sProperty)
			{
				case "Content":
				return $this->m_sContent; 
				break;
				
				case "TweetDate":
				return $this->m_dTweetDate;
				break;
				
				case "User":
				return $this->m_sUser;
				break;
				
			}
			
		}	
	
		function save()
		{
			$db = new Db();
			$sql = "insert into imdtalks_tweet (content, date) values ('$this->m_sContent','$this->m_dTweetDate' );";
			$db->conn->query($sql);
			return true;
		}
		
		function getAll()
		{
			$db = new Db();
			$sql = 'select content, date from imdtalks_tweet;';
			$r = $db->conn->query($sql);
			return $r;
			
			
		}
		
		
		
	}
?>