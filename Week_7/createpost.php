<?php
	
		include_once("classes/Tweet.class.php");
		$tweet = new Tweet();
		if(isset($_POST['btnCreatePost']))
		{
			try
			{
				$tweet->Text = $_POST['post'];
				$tweet->UserId = 1; //get this from session instead of hardcoded!
				$tweet->Save();
				$feedback['text'] = "Your tweet has been posted!";
				$feedback['status'] = "success";
			}
			catch(Exception $e)
			{
				$feedback['text'] = $e->getMessage();
				$feedback['status'] = "error";
			}
		}

		$allTweets = $tweet->getAll();

?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>IMD Talks</title>
	<link rel="stylesheet" href="css/normalize.css" media="all">
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>
	<nav>
		<a href="twitter_logout.php"Logout>Logout</a>
	</nav>
	
	<div id="container">	
	<section id="newpost">
		<form action="" method="post">
			<label for="post" id="feedback">What's on your mind?</label>	
			<textarea name="post" id="post" cols="30" rows="2"></textarea>		
			<input type="submit" name="btnCreatePost" id="btnCreatePost" value="Send" />
			<img id="loading" src="images/loading.gif" />
			
			
		</form>
	</section>
	
	<section id="tweets">
		<h2>Your Updates</h2>
		
		<ul>
		<?php if(count($allTweets) > 0): ?>
		<?php foreach($allTweets as $tweet){ ?>
			<li class="clearfix">
				<img class="avatar" src="images/avatar.jpg" />
				<p><?php echo $tweet['text'] . " <span>" . $tweet['date_posted'] . "</span>"; ?></p>
			</li>
		<?php } ?>
		<?php else: ?>
		<li id="noposts">Oops, there are no posts yet.</li>
		<?php endif; ?>
		</ul>
		
		
		
	</section>
	
	</div>	
	
	<!-- scripts at the bottom for faster loading -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="js/app.js"></script>
	
	
</body>
</html>