<?php
require_once("Db.class.php");
class Tweet
{
	// PRIVATE MEMBER VARIABLES
	private $m_sText;
	private $m_iUserId;

	public function __set($p_sProperty, $p_vValue)
	{
		switch($p_sProperty)
		{
			case "Text":
				if(strlen($p_vValue) > 50)
				{
					throw new Exception("A tweet can be no longer than 50 characters.");
				}
				else
				{
					$this->m_sText = $p_vValue;
				}
			break;

			case "UserId":
				$this->m_iUserId = $p_vValue;
			break;

			default: echo "can't find property named " . $p_sProperty;
		}
	}

	public function Save()
	{
		$db = Db::getInstance();
		$table = "imdtalks_tweets";
		$cols = array("text", "fk_user_id", "date_posted");
		$values = array($this->m_sText, $this->m_iUserId, "NOW()");
		$db->insert($table, $cols, $values);
	}

	public function GetAll()
	{
		$db = Db::getInstance();
		$cols = array("*");
		$result = $db->select($cols, "imdtalks_tweets", null, "id", "DESC");
		return($result);
	}

}
?>