# ************************************************************
# Sequel Pro SQL dump
# Version 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.27)
# Database: php4wa
# Generation Time: 2013-04-29 09:40:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tblMovies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblMovies`;

CREATE TABLE `tblMovies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `movie` varchar(255) DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `poster` varchar(255) DEFAULT NULL,
  `description` text,
  `rating` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblMovies` WRITE;
/*!40000 ALTER TABLE `tblMovies` DISABLE KEYS */;

INSERT INTO `tblMovies` (`id`, `movie`, `genre`, `poster`, `description`, `rating`)
VALUES
	(1,'Les Chevaux de Dieu','drama','1.jpg','In zijn excellente broederdrama ‘Les Chevaux de Dieu’ laat de Marokkaanse regisseur Nabil Ayouch van binnenuit zien hoe een kansarme omgeving als vanzelf een broeihaard voor extremisme kan worden.',7),
	(2,'Django Unchained','thriller','2.jpg','Ach ja, de jodenvervolging had hij al gehad in ‘Inglourious Basterds’, een postmoderne film over de slavernij kon er dus ook nog wel bij voor Quentin Tarantino, uiteraard nooit verlegen om flink wat controverse.\nHet resultaat heet ‘Django Unchained’, en bouwt voort op het verhaal van Sergio Corbucci’s spaghettiwestern ‘Django’, een cultklassieker uit 1966: de zwarte slaaf Django wordt vrijgekocht door de blanke dokter Schultz. Samen proberen ze Django’s vrouw te bevrijden uit de klauwen van slavendrijver Calvin Candie en zijn demonische “huisneger” Stephen, die niks liever doet dan zelf de zweep er flink over te leggen.',8),
	(3,'Moonrise Kingdom','romance','3.jpg','Wat maakt Wes Anderson toch héérlijke films: Technicolor-pareltjes die constant tussen lichte weemoed en bitterzoete humor twijfelen, en die verfilmd zijn in een smetteloze stijl. Zie ‘The Royal Tenenbaums’, zie ‘The Darjeeling Limited’, zie ‘The Life Aquatic’ en zie – in het Filmhuis, uiteraard! – nu ook het wonderlijke ‘Moonrise Kingdom’.\nHet verhaal speelt zich af in 1965, wanneer de kleine Sam (wees, brildrager en scoutsjongen) verliefd wordt op Suzy (fan van verrekijkers en van Françoise Hardy). De twee besluiten te vluchten naar New Penzance, een eiland vol ongerepte natuur. Maar dat is uiteraard buiten de volwassenen gerekend, die met zijn allen de achtervolging inzetten: van de scoutsleider (Edward Norton!) tot Suzies ouders (Bill Murray en Frances McDormand!), en van politiekapitein Sharp (Bruce Willis!) tot het takkewijf van de Dienst Jeugdzorg (Tilda Swinton!).',8),
	(4,'The Master','drama','4.jpg','Met het geweldige trio ‘Boogie Nights’, ‘Magnolia’ en ‘There Will Be Blood’ scoorde Paul Thomas Anderson een echte hattrick, en dus tonen wij u ook heel graag zijn al even magistrale, epische vierde: ‘The Master’.\n\nCentrale figuur is Freddie Quell (een grootse Joaquin Phoenix), een soldaat die na WOII compleet getraumatiseerd zijn leven weer probeert op te bouwen.',6),
	(5,'Killer Joe','thriller','5.jpg','Zegt ‘The Exorcist’ u iets? ‘The French Connection’? Twee onbetwiste meesterwerken uit de seventies, en allebei geregisseerd door William Friedkin. Friedkin is ondertussen 78 (!), maar nog zéér alive and kicking, getuige zijn werkelijk excellente nieuwe ‘Killer Joe’.\n\nDeze inktzwarte vitrioolkomedie speelt zich af bij de familie Smith, uitschot van de ergste soort. Zoon Chris heeft bij zijn dealer een drugsschuld van enkele duizenden dollar, en komt daarom op het briljante idee zijn moeder te laten vermoorden, om zo de levensverzekering te kunnen opstrijken.',7);

/*!40000 ALTER TABLE `tblMovies` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
