<?php
	include_once("classes/Movie.class.php");

	$m = new Movie();
	$r = $_GET['r'];
	
	if (!$r){
	$movies = $m->getAll();
	} else if ($r == "all"){
		$movies = $m->getAll();	
	} else 
	{
		$movies = $m->GetAllByRating($r);	
	}
		
?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>AJAX</title>
	
	<link rel="stylesheet" href="css/normalize.css" />
	<link rel="stylesheet" href="css/app.css" />
</head>
<body>	

	<section id="mood">
		<h1>Movie Mood</h1>

		<nav>
			<li><a data-rating="6" href="?r=6">&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;</a></li>
			<li><a data-rating="7" href="?r=7">&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;</a></li>
			<li><a data-rating="8" href="?r=8">&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;</a></li>
			<li><a data-rating="all" href="?r=all">Alle Films</a></li>
		</nav>
		
		<h2>Punten verdeling</h2>
		<ul id="scores">
			<li>Films worden gefilterd via AJAX (10p)</li>
			<li>"Alle films" tonen via AJAX werkt (4p)</li>
			<li>Toepassing werkt ook zonder JS (progressive enhancement) (4p)</li>
			<li>Pas een effect toe op het vertonen van films (fadeIn) (2p)</li>
		</ul>
		
		
		<p class="notice">
			Teksten en afbeeldingen - 
			<a href="Fair Use Policy">Fair Use Policy</a>  
			<a href="http://www.filmhuismechelen.be/">Filmhuis Mechelen</a> - <a href="http://www.imdb.com">IMDB</a>
		</p>
		
	</section>

	<section id="movies">
		<ul>
		<?php foreach($movies as $m){ ?>
			<li>
				<h2><?php echo $m['movie']; ?></h2>
				<img src="posters/<?php echo $m['id']; ?>.jpg" alt="<?php echo $m['movie']; ?>">
				<p><?php echo htmlspecialchars($m['description']); ?></p>
			</li>
		<?php } ?>
		</ul>
	</section>



	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>