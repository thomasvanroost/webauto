<?php
class Movie
{

	private $db_server = "localhost";
	private $db_user = "root";
	private $db_password = "";
	private $db_database = "db_lesphp";

	public function GetAll()
	{
		$conn = new mysqli($this->db_server, $this->db_user, $this->db_password, $this->db_database);
		$conn->set_charset("utf8");
		$query = "select * from tblMovies";
		$result = $conn->query($query);
		$result_array=array();

		while($row = $result->fetch_array())
		{
			$result_array[] = $row;
		}

		return($result_array);
	}


	public function GetAllByRating($rating)
	{
		$conn = new mysqli($this->db_server, $this->db_user, $this->db_password, $this->db_database);
		$conn->set_charset("utf8");
		$query = "select * from tblMovies where rating = '".$conn->real_escape_string($rating)."';";
		$result = $conn->query($query);
		$result_array=array();

		while($row = $result->fetch_array())
		{
			$result_array[] = $row;
		}

		return($result_array);
	}
}
?>