<?php
include_once 'inc_array.php';

//var_dump($_POST['aantal'] );
if(isset($_POST['aantal']) && !empty($_POST['aantal'])) 
{
	$howMany = $_POST['aantal'];
	$feedback = "Here are $howMany artists";  // je kan een var in een string verwerken maar dan alleen onder dubble " ".
} else {
	$feedback = "Enter a number please.";	
}


//var_dump($feedback);
?>


<!doctype html>
<html lang="en">
<head>
<style type="text/css">

@charset "utf-8"; 
/* CSS Document for 2 Interactive Multimedia Design */
/* 
   PINK #e0317d
   DARK BROWN #a67858
   LIGHT BROWN #f1cbb2
   DARK PURPLE #40212c
*/

*
{
	margin: 0;
	padding: 0;
}

body
{
		background-color:#EEE;
		font-family:Tahoma, Geneva, Arial, sans-serif;
}

h1
{
	font-weight: normal;
	font-size: 24px;
}

img.thumbnail
{
	border: 2px solid #fff;
	width: 400px;
	height: 400px;
	float:right;
	margin-top: 20px;
}

input
{
	font-size: 20px;
	font-weight:bold;
	border: 1px solid #999;
	padding: 10px;
	margin: 5px 0px;
}

select
{
	font-size: 18px;
	padding: 4px;
}

div.center, div.results
{
	margin-left: auto;
	margin-right: auto;
	width: 600px;
	text-align:center;
}

form
{
	width: 500px;
	text-align:right;
}

div.feedback
{
	width: 600px;
	background-color: #e0317d;
	color: #fff;
	border: 2px solid #fff;
	font-size: 20px;
	padding: 10px;
	margin: 20px auto;
}

table
{
	background-color: #a67858;
	color: #fff;
	border: 4px solid #f1cbb2;
	font-size: 20px;
	border-collapse:collapse;
}

table tr
{
	margin: 0px;
	padding: 0px;
}

table tr td
{
	border: 1px dotted #f1cbb2;	
	padding: 5px;
}

hr
{
	line-height:1px;
	color: red;
	margin: 10px 0px;
}

div.loading
{
	width: 35px;
	height: 35px;
	background-image: url(../les3/images/loading.gif);
	display:none;
}

div.cv
{
	margin: 15px;
}</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GET vs POST</title>
</head>

<body>

<div class="feedback">
<!-- here comes form feedback -->
<?php echo $feedback ?>


</div>

<div class="center">
<form action="<?php echo $_SERVER[PHP_SELF] ?>" method="post">
<div>
<input id="test" name="aantal" type="text" />
<input id="verzendknop" value="Zoek Artist" type="submit" />
</div>
</form>

<?php

if(isset($howMany)){
echo "<table>";
 
for($i=0; $i<$howMany; $i++){
	echo "<tr>";
	echo "<td> <a href='artistdetail.php?id=$i'> " .$artist[$i]['name'].   "</td>";
	
	echo "<tr>";
}

echo "</table>";
}
?>


</div>

</body>
</html>