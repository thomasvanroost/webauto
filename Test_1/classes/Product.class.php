<?php
	include_once("db.class.php");
	class Product
	{
		private $m_sName;			// Product X
		private $m_iHowMany; 		// 5
		private $m_sType;			// new

		public function __set($p_sProperty, $p_vValue)	 	
		{
			switch ($p_sProperty) {
				case 'Name':
					$this->m_sName = mysql_real_escape_string($p_vValue);
					break;
				
				case 'HowMany':
				
					if ($p_vValue > 0){
					
	
					
					$this->m_iHowMany = mysql_real_escape_string($p_vValue);
					
					
					
					} else {
						throw new Exception("Het aantal moet groter zijn dan 0");
					}
					
					
					break;

				case 'Type':
					$this->m_sType = mysql_real_escape_string($p_vValue);
					break;				
			}
		}

		public function __get($p_sProperty)
		{
			switch($p_sProperty)
			{
				case 'Name':
					return($this->m_sName);
					break;

				case 'HowMany':
					return($this->m_iHowMany);
					break;

				case 'Type':
					return($this->m_sType);
					break;
			}
		}


		public function Save()
		{
			try 
		{
			switch ($this->m_sType){
				case 'new':
				if ($this->m_iHowMany > 20){
					throw new Exception("max 20 nieuwe producten per keer");
				}
				break;
				
				case 'return';
					if ($this->m_iHowMany > 5){
					throw new Exception("max 5 tweedehands producten per keer");
				} 
				break;
			}
		
			$db = new Db();
			$sql = ("insert into OOPtblorders_v9 (NAME, howmany, type)
					values ('$this->m_sName', '$this->m_iHowMany', '$this->m_sType');");
			$db->conn->query($sql);
			if ($db){
				return "Product opgeslagen";
			}
			}
			
			catch (Exception $e)
			{
				throw new Exception($e->getMessage()); 
				
			}

			
		}

		public function GetAll()
		{
			$db = new Db();
			$sql = ("select name, howmany, type
			from OOPtblorders_v9;");
			$result = $db->conn->query($sql);
			return $result;
		}
	}


?>