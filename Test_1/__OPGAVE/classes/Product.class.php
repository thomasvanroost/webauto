<?php

	class Product
	{
		private $m_sName;			// Product X
		private $m_iHowMany; 		// 5
		private $m_sType;			// new

		public function __set($p_sProperty, $p_vValue)
		{
			switch ($p_sProperty) {
				case 'Name':
					$this->m_sName = $p_vValue;
					break;
				
				case 'HowMany':
					$this->m_iHowMany = $p_vValue;
					break;

				case 'Type':
					$this->m_sType = $p_vValue;
					break;				
			}
		}

		public function __get($p_sProperty)
		{
			switch($p_sProperty)
			{
				case 'Name':
					return($this->m_sName);
					break;

				case 'HowMany':
					return($this->m_iHowMany);
					break;

				case 'Type':
					return($this->m_sType);
					break;
			}
		}


		public function Save()
		{

		}

		public function GetAll()
		{
		
		}
	}


?>