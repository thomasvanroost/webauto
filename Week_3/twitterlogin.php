<?php
	$salt = "lalalalajahaa";
	
	define("DB_SERVER", "localhost");
	define("DB_USER", "root");
	define("DB_PASSW", "");
	define("DB_NAME", "db_lesphp");	

if(isset($_POST['btnSignup'])){
	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = MD5($_POST['password'].$salt);	
	
	$conn = new mysqli(DB_SERVER, DB_USER, DB_PASSW, DB_NAME);
	if (!$conn->connect_errno)
	{
		echo "connectie ok";
		$sql = "insert into imdtalks_user(name, email, password) values('$name', '$email', '$password');";	
	}
	
	if ($_SESSION['loggedin'] == !true){
	if($conn->query($sql))
			{
				// QUERY OK;
				session_start();
				$_SESSION['name'] = $name;
				$_SESSION['loggedin'] = true;
				header("location: loggedin.php");	
			}
	}
}


?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>IMD Talks</title>
	<link rel="stylesheet" href="normalize.css" media="all">
	<style>
	html{ width: 100%; height: 100%; }
	body{ 
		background-image: url(https://abs.twimg.com/a/1362015167/t1/img/front_page/jp-mountain@2x.jpg);
		background-size: cover; 
		width: 100%;
		height: 100%;
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	}

	header{
		margin-top: 200px;
		margin-left: 10%;
		margin-right: 10%;
		width: 30%;
		float: left;
	}

	header h1{ color: #fff; font-size: 26px; font-weight: normal;margin-bottom: 0em; text-shadow: 1px 1px 0 #000;}
	header h2{ color: #fff; font-size: 20px; font-weight: normal;margin-top: 0em; text-shadow: 1px 1px 0 #000}

	#signup, #login
	{
		margin-right: 10%;
		padding: 10px 5%;
		background-color: #f5f5f5;
		border-radius: 10px;
		margin-top: 30px;
	}

	#rightside{ width: 40%;	padding: 10px 0; float: left; margin-top: 60px;}
	#signup h2{ font-weight: normal; font-size: 18px; border-bottom: 1px solid #e2e2e2; }
	#signup input, #login input{ display: block; width: 70%; margin-bottom: 10px; border-radius:5px; border: 1px solid #cccccc; padding: 5px; }
	#signup input[type=submit]{ background-color: #fdbd2d; border: 1px solid #fd9e28; border-radius: 5px; width: 200px; font-size: 14px; font-weight: bold; text-shadow: 1px 1px 0 #fee2bf;}
	#login input[type=submit]{ background-color: #2bb6ea; border: 1px solid #2092ca; border-radius: 5px; width: 100px; font-size: 14px; color: #fff; font-weight: bold; text-shadow: -1px -1px 0 #2092ca;}
	#login input[type=checkbox]{ display: inline; width: auto; }
	nav{background-color: #2d2d2d; padding: 8px 10px; text-align: right; }
	nav a{ color: #bbbbbb; font-size: 14px; text-decoration: none;} 
	label{ color: #999999; font-size: 14px; }
	</style>
	
</head>
<body>
	<nav>
		<a href="twitter_logout.php"Logout>Logout</a>
	</nav>

	<header>
		<h1>Welcome to IMD-Talks</h1>
		<h2>Find out what other IMD'ers are building around you.</h2>
	</header>
	
	<div id="rightside">	
	<section id="login">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<input type="text" name="username" placeholder="Email" />
		<input type="password" name="password" placeholder="Password" />
		<input type="submit" name="btnLogin" value="Sign in" />
		<input type="checkbox" name="rememberme" value="yes" id="rememberme">
		<label for="rememberme">Remember me</label>
		</form>
		
	</section>	
	
	<section id="signup">
	
		<h2>New to IMD-Talks? <span>Sign Up</span></h2>
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<input type="text" name="name" placeholder="Full name" />
		<input type="email" name="email" placeholder="Email" />
		<input type="password" name="password" placeholder="Password" />
		<input type="submit" name="btnSignup" value="Sign up for IMD Talks" />
		</form>
		
	</section>
	</div>	
	
</body>
</html>