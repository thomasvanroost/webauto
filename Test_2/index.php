<?php
	include_once("classes/Movie.class.php");
	
	$m = new Movie();
	
	if (isset($_GET['id'])){
		$m->MakeFavourite($_GET['id']);
	}
	
	
	
	if(isset($_GET['filter']))
	{
		if($_GET['filter'] == "fav")
		{
			$movies = $m->GetAll("yes"); // GET FAVOURITE MOVIES
		}
	}
	else
	{
		$movies = $m->GetAll();
	}
	
	

?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>AJAX</title>
	
	<link rel="stylesheet" href="css/normalize.css" />
	<link rel="stylesheet" href="css/app.css" />
</head>
<body>	

	<section id="mood">
		<h1>Movie Mood</h1>

		<nav>
			<li><a href="index.php?filter=fav">Mijn favorieten</a></li>
			<li><a data-rating="all" href="index.php">Alle Films</a></li>
		</nav>
		
		<h2>Punten verdeling</h2>
		<ul id="scores">
			<li>Klik op een film markeert de film als "favoriet" in de databank via AJAX (10p)</li>
			<li>De groene kleur (klasse .fav_yes) wordt getoond zodra een film als favoriet wordt aangevinkt (4p)</li>
			<li>Een film als favoriet aanvinken werkt ook zonder JS (progressive enhancement) (6p)</li>
			<li>EXTRA: (2 BONUS PUNTEN) indien ook het UITvinken van favorieten mogelijk is</li>
		</ul>
		
		
		<p class="notice">
			Teksten en afbeeldingen - 
			<a href="Fair Use Policy">Fair Use Policy</a>  
			<a href="http://www.filmhuismechelen.be/">Filmhuis Mechelen</a> - <a href="http://www.imdb.com">IMDB</a>
		</p>
		
	</section>

	<section id="movies">
		<ul>
		<?php foreach($movies as $m){ ?>
			<li>
			<a class="fav_<?php echo $m['favourite'] ?>" href="<?php echo "index.php?id=" .$m['id'] ; ?>" rel="<?php echo $m['id']; ?>" data-movieid="<?php echo $m['id']; ?>">
			<img src="posters/<?php echo $m['id']; ?>.jpg" alt="<?php echo $m['movie']; ?>">
			</div>
			</a>
			</li>
			
		<?php } ?>
		</ul>
	</section>



	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>