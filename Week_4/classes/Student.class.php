<?php
	include_once("Db.class.php");
	
	class Student
	{
		
		private $m_sNaam;
		private $m_sVoornaam;
		private $m_iGeboortejaar;
		
						
						
		public function __get($p_sProperty)
		{
			
			switch($p_sProperty)
			{
				case "Naam":
				return $this->m_sNaam;
				break;
				
				case "Voornaam":
				return $this->m_sVoornaam;
				break;
				
				case "Geboortejaar":
				return $this->m_iGeboortejaar;
				break;
				
				//default:
				//throw new Exception($p_sProperty . ' is niet gevonden');
							
			}
		}
						
		public function __set($p_sProperty, $p_vValue) // magic function die weet dat je wil setten.
		{
			
			switch($p_sProperty)
			{
				case "Naam":
				$this->m_sNaam = $p_vValue;				
				break;
				
				case "Voornaam":
				$this->m_sVoornaam = $p_vValue;				
				break;
				
				case "Geboortejaar":
				if (is_numeric($p_vValue) && $p_vValue > (date("Y") - 99) && $p_vValue < (date("Y") - 18) )
				{
					$this->m_iGeboortejaar = $p_vValue;				
				} else if ($p_vValue < (date("Y") - 130) ) {
					echo " Te oud";
				} else {
					echo "Te jong";
				}
				break;
				
			//	default:
				//throw new Exception($p_sProperty . ' is niet gevonden');
			}
		}
		
		
		public function save()
		{
		try 
		{
			$db = new Db();
			//$sql = "insert into...";
			$db->conn->query($sql);
			}
			
			catch (Exception $e)
			{
				
				
			}
		}
		
		
		public function __toString()
		{
			$student = "<h1>" . $this->m_sNaam . "</h1>"; 
			$student .= "<h2>" . $this->m_sVoornaam . "</h2>";
			$student .= "<p>" . $this->Geboortejaar . "</p>";							
			return $student;
		}
		
			
	}


?>