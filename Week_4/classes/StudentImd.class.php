<?php
	include_once("Student.class.php");


	class StudentImd extends Student {
		private $m_iPhotoshop;
		
	
		public function __set($p_sProperty, $p_vValue)
		{
			parent::__set($p_sProperty, $p_vValue);  //parent spreekt een functie in een andere klasse aan.
			
			switch($p_sProperty)
			{
				case "Photoshop":
				$this->m_iPhotoshop = $p_vValue;
				break;
			}
			
		}
	
		public function __get($p_sProperty)
		{
			$vResult = parent:: __get($p_sProperty);
			
			switch($p_sProperty)
			{
				case "Photoshop":
				$this->m_iPhotoshop = $p_vValue;
				$vResult = $this->m_iPhotoshop;
				break;
			}
			return $vResult;
		}
	
	public function __toString()
	{
		$student = parent:: __toString();
		$student .= "<p> Photoshop: " . $this->m_iPhotoshop . "</p>";
		return $student;
	}
	
	
	}

?>