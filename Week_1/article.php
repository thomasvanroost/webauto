<!doctype html>


<html>
	<head>
		<meta charset="UTF-8" />
		<title>Homepage</title>
	</head>
	<body>
		
		
		<?php 
			include("include_nav.php");
		?>
		
		<h1>Article</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio dolorem est laudantium rem iste necessitatibus consequuntur ipsa temporibus modi architecto hic minima veritatis quaerat aliquam incidunt explicabo inventore harum nihil.</p>
		
		<?php 
			//include once is beter
			// omdat dit vermijd dat je 2 maal een zelfde include doet
			include_once("include_footer.php");
		?>
		
		
	</body>
</html>